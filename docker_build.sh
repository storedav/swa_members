SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# build docker image
docker build -t divad4a/cvut_fel_swa_members $SCRIPT_DIR

#push docker image to hub
docker push divad4a/cvut_fel_swa_members:latest