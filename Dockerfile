FROM maven:latest as builder
ENV APP_HOME=/root/dev/app/
ENV DEPS_HOME=/root/dev/deps

WORKDIR $DEPS_HOME
ADD --keep-git-dir=true https://gitlab.fel.cvut.cz/storedav/swa_interfaces.git $DEPS_HOME/swa_interfaces
WORKDIR $DEPS_HOME/swa_interfaces
RUN mvn -Dmaven.test.skip install

RUN mkdir -p $APP_HOME/src
WORKDIR $APP_HOME
COPY ./src $APP_HOME/src
COPY pom.xml $APP_HOME/pom.xml
RUN mvn package
RUN ls -la ./target
RUN cp ./target/SWA_members*.war ./target/SWA_members.war

FROM openjdk:17-jdk-alpine
LABEL Maintainer: storedav@fel.cvut.cz
ENV APP_HOME=/root/dev/app/
ENV DEPS_HOME=/root/dev/deps
WORKDIR /usr/app/
COPY --from=builder $APP_HOME/target/SWA_members.war ./SWA_members.war
ENTRYPOINT ["java", "-jar", "/usr/app/SWA_members.war"]