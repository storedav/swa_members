@REM Find path of current file
set SCRIPT_DIR=%~dp0

@REM build docker image
docker build -t divad4a/cvut_fel_swa_members %SCRIPT_DIR%

@REM push docker image to hub
docker push divad4a/cvut_fel_swa_members:latest