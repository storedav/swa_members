DELETE FROM members;
ALTER TABLE members ALTER COLUMN id RESTART WITH 1;

INSERT INTO members (name, status, licenses) VALUES ('David S', 'active', 'ull');
INSERT INTO members (name, status, licenses) VALUES ('Alice', 'active', 'ppl');
INSERT INTO members (name, status, licenses) VALUES ('Bob', 'student', 'ull');
INSERT INTO members (name, status, licenses) VALUES ('Karel', 'instructor', 'ull;ppl');