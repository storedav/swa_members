import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;
import cz.fel.storedav.swa.members.Application;
import cz.fel.storedav.swa.members.api.ReservationsServiceApiClient;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import javax.annotation.PostConstruct;
import org.springframework.cloud.openfeign.EnableFeignClients;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static io.restassured.RestAssured.get;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"reservationsreg.url=http://localhost:9093", "reservationsreg.name=reservations-register"}
)
@EnableConfigurationProperties
public class MembersApiDelegateIT {
    @LocalServerPort
    private int port;

    private String uri;

    @Autowired
    private ReservationsServiceApiClient reservationsServiceApiClient;

    @RegisterExtension
    static WireMockExtension ext = WireMockExtension.newInstance()
            .options(wireMockConfig().port(9093))
            .configureStaticDsl(true)
            .build();

    @PostConstruct
    public void init() {
        uri = "http://localhost:" + port;
    }

    @BeforeEach
    public void testSetup(){
        RestAssured.port = port;
    }

    @Test
    @Sql(scripts = "/test-data-empty.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_getMembers_noData() {
        get(uri+"/members").then().assertThat().statusCode(204);
    }

    @Test
    @Sql(scripts = "/test-data-1.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_getMembers_listOfShorts(){
        RestAssured.given().when()
                .get("/members")
                .then()
                .statusCode(200)
                .body("[0].name", equalTo("David S"))
                .body("[0].member", equalTo(uri+"/member/1"))
                .body("[3].name", equalTo("Karel"))
                .body("[3].member", equalTo(uri+"/member/4"));
    }

    @Test
    @Sql(scripts = "/test-data-1.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_postMember_getMembers_listOfShorts(){
        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"name\": \"Charlie\",\n" +
                        "  \"status\": \"grounded\",\n" +
                        "  \"licenses\": [\n" +
                        "    \"ull\",\n" +
                        "    \"ppl\"\n" +
                        "  ]\n" +
                        "}")
                .when()
                .post("/members")
                .then()
                .statusCode(201);

        RestAssured.given().when()
                .get("/members")
                .then()
                .statusCode(200)
                .body("[4].name", equalTo("Charlie"))
                .body("[4].member", equalTo(uri+"/member/5"));
    }

    @Test
    @Sql(scripts = "/test-data-1.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_postMember_duplicate(){
        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"name\": \"Alice\",\n" +
                        "  \"status\": \"grounded\",\n" +
                        "  \"licenses\": [\n" +
                        "    \"ull\",\n" +
                        "    \"ppl\"\n" +
                        "  ]\n" +
                        "}")
                .when()
                .post("/members")
                .then()
                .statusCode(409);
    }


    @Test
    @Sql(scripts = "/test-data-1.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_putMemberStatus(){

        stubFor(WireMock.post(WireMock.urlEqualTo("/notify/change/member"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())));

        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"status\": \"grounded\",\n" +
                        "  \"licenses\": [\n" +
                        "    \"ull;ppl\"\n" +
                        "  ]\n" +
                        "}")
                .when()
                .put("/member/1/state")
                .then()
                .statusCode(202);

        verify(postRequestedFor(urlEqualTo("/notify/change/member"))
                .withHeader("Content-Type", com.github.tomakehurst.wiremock.client.WireMock.equalTo("application/json")));
    }

}
