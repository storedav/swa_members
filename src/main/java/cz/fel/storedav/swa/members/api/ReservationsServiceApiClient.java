package cz.fel.storedav.swa.members.api;

import cz.fel.storedav.swa.reservations_client.api.ReservationsApiClient;
import cz.fel.storedav.swa.reservations_client.api.ReservationsApi;
import cz.fel.storedav.swa.reservations_client.model.MemberStatusDto;
import cz.fel.storedav.swa.reservations_client.model.ReservationDto;
import cz.fel.storedav.swa.reservations_client.model.ReservationShortDto;
import cz.fel.storedav.swa.reservations_client.model.URIMemberStatusDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.time.OffsetDateTime;
import java.util.List;

@FeignClient(
        contextId = "Reservations",
        name = "${reservationsreg.name:reservations-register}",
        url =  "${reservationsreg.url}",
        fallback = ReservationsServiceApiClient.ReservationsServiceApiClientFallback.class,
        configuration = ReservationsApiConfiguration.class
)
public interface ReservationsServiceApiClient extends ReservationsApi {
    Logger logger = LoggerFactory.getLogger(ReservationsServiceApiClient.class);

    @Component
    class ReservationsServiceApiClientFallback implements ReservationsServiceApiClient {

        @Override
        public ResponseEntity<ReservationDto> reservationReservationIdGet(Integer reservationId) {
            return ResponseEntity.notFound().build();
        }

        @Override
        public ResponseEntity<Void> notifyChangeMemberPost(URIMemberStatusDto urIMemberStatusDto) {
            logger.warn("fallback notifyChangeMemberPost");
            return ResponseEntity.internalServerError().build();
        }

        @Override
        public ResponseEntity<List<ReservationShortDto>> reservationsGet(OffsetDateTime startDate, OffsetDateTime endDate, URI member, URI plane) {
            logger.warn("fallback reservationsGet");
            throw new UnsupportedOperationException("reservationsGet should not be called here...");
        }

        @Override
        public ResponseEntity<URI> reservationsPost(ReservationDto reservationDto) {
            logger.warn("fallback reservationsPost");
            throw new UnsupportedOperationException("reservationsPost should not be called here...");
        }
    }
}
