package cz.fel.storedav.swa.members.model.mappers;

import cz.fel.storedav.swa.api.model.MemberDto;
import cz.fel.storedav.swa.api.model.MemberShortDto;
import cz.fel.storedav.swa.api.model.MemberStatusDto;
import cz.fel.storedav.swa.members.model.Member;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.web.util.UriTemplate;

@Mapper
public interface MemberMapper {

    public static MemberMapper INSTANCE = Mappers.getMapper(MemberMapper.class);

    @Mapping(target="name", source = "member.name")
    @Mapping(target="status", source = "member.status")
    @Mapping(target="licenses", expression = "java(Member.toLicensesList(member.getLicenses()))")
    MemberDto mapTo(Member member);

    @Mapping(target="name", source = "member.name")
    @Mapping(target="member", expression = "java(base_uri.expand(member.getId()))")
    MemberShortDto mapToShort(Member member, UriTemplate base_uri);

    @Mapping(target="status", source = "member.status")
    @Mapping(target="licenses", expression = "java(Member.toLicensesList(member.getLicenses()))")
    MemberStatusDto mapToStatus(Member member);


    @Mapping(target="name", source = "memberDto.name")
    @Mapping(target="status", source = "memberDto.status")
    @Mapping(target="licenses", expression = "java(Member.toLicenses(memberDto.getLicenses()))")
    Member mapTo(MemberDto memberDto);
}
