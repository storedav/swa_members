package cz.fel.storedav.swa.members.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "members")
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    @NotEmpty
    private String name;

    @Column(name = "status")
    @Pattern(regexp = "^active|student|grounded$")
    private String status;

    @Column(name = "licenses")
    private String licenses;


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getLicenses() {
        return licenses;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLicenses(String licenses) {
        this.licenses = licenses;
    }

    public static List<String> toLicensesList(String licenses){
        return Arrays.stream(licenses.split(";")).toList();
    }
    public static String toLicenses(List<String> licenses) {
        return String.join(";", licenses);
    }

}
