package cz.fel.storedav.swa.members.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemberRepository extends JpaRepository<Member, Integer>{
    long countByName(String registration);

    List<Member> findByNameLikeAndLicensesLikeAndStatusLike(String name, String licenses, String Status);
}
