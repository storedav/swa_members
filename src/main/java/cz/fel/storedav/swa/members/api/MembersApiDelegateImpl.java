package cz.fel.storedav.swa.members.api;

import cz.fel.storedav.swa.api.MembersApiDelegate;
import cz.fel.storedav.swa.api.model.MemberShortDto;
import cz.fel.storedav.swa.members.model.Member;
import cz.fel.storedav.swa.members.model.MemberRepository;
import cz.fel.storedav.swa.members.model.mappers.MemberMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import cz.fel.storedav.swa.api.model.MemberDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriTemplate;

import java.net.URI;
import java.util.List;

@Service
public class MembersApiDelegateImpl implements MembersApiDelegate{

    final Logger logger = LoggerFactory.getLogger(MembersApiDelegateImpl.class);

    private final MemberRepository membersRepo;

    public MembersApiDelegateImpl(MemberRepository membersRepo) {
        this.membersRepo = membersRepo;
    }

    @Override
    public ResponseEntity<List<MemberShortDto>> membersGet(String name, String license, String status) {

        logger.trace("GET memebrs list");

        logger.trace("name: " + name + ", license: " + license + ", status: " + status);

        List<Member> foundMembers = membersRepo.findByNameLikeAndLicensesLikeAndStatusLike(
                name == null || name.isEmpty() ? "%" : name,
                license == null || license.isEmpty() ? "%" : license,
                status == null || status.isEmpty() ? "%" : status);

        if (foundMembers.isEmpty()) {
            throw new ResponseStatusException(
                    HttpStatus.NO_CONTENT, "No member with specified parameters was found."
            );
        }

        final UriTemplate memberUriTeamplate = new UriTemplate(ServletUriComponentsBuilder.fromCurrentContextPath().path("/member/").toUriString() + "{memberId}");

        final List<MemberShortDto> members = foundMembers.stream().map(
                member -> MemberMapper.INSTANCE.mapToShort(member, memberUriTeamplate)
        ).toList();

        return ResponseEntity.ok(members);
    }

    @Override
    public ResponseEntity<URI> membersPost(MemberDto memberDto) {
        logger.trace("PUT new member into members list");

        logger.trace("Member name: " + memberDto.getName());

        if (membersRepo.countByName(memberDto.getName()) > 0) {
            logger.info("Member with registration " + memberDto.getName() + " already exists.");
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Other member with the same name already exists."
            );
        }

        Member member = MemberMapper.INSTANCE.mapTo(memberDto);

        Member savedMember = membersRepo.save(member);

        final URI memberUri =
                ServletUriComponentsBuilder.fromCurrentContextPath().path("/member/").path(savedMember.getId().toString()).build().toUri();
        return ResponseEntity.created(memberUri).body(memberUri);
    }
}
