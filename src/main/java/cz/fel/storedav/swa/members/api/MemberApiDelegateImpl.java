package cz.fel.storedav.swa.members.api;

import cz.fel.storedav.swa.api.MemberApiDelegate;
import cz.fel.storedav.swa.members.model.Member;
import cz.fel.storedav.swa.members.model.MemberRepository;
import cz.fel.storedav.swa.members.model.mappers.MemberMapper;
import cz.fel.storedav.swa.reservations_client.model.URIMemberStatusDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import cz.fel.storedav.swa.api.model.MemberDto;
import cz.fel.storedav.swa.api.model.MemberStatusDto;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@Service
public class MemberApiDelegateImpl implements MemberApiDelegate{

    final Logger logger = LoggerFactory.getLogger(MemberApiDelegateImpl.class);

    private final MemberRepository membersRepo;

    @Autowired
    private ReservationsServiceApiClient reservationsServiceApiClient;

    public MemberApiDelegateImpl(MemberRepository membersRepo) {
        this.membersRepo = membersRepo;
    }

    @Override
    public ResponseEntity<MemberDto> memberMemberIdGet(Integer memberId) {
        logger.trace("GET member request, id: " + memberId);
        Optional<Member> plane = membersRepo.findById(memberId);

        if (plane.isEmpty()) {
            logger.info("Member " + memberId + " not found.");
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Member not found."
            );
        }
        return ResponseEntity.ok(MemberMapper.INSTANCE.mapTo(plane.get()));
    }

    @Override
    public ResponseEntity<MemberStatusDto> memberMemberIdStateGet(Integer memberId) {
        logger.trace("GET member status request, id: " + memberId);
        Optional<Member> member = membersRepo.findById(memberId);

        if (member.isEmpty()) {
            logger.info("Member " + memberId + " not found.");
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Member not found."
            );
        }
        return ResponseEntity.ok(MemberMapper.INSTANCE.mapToStatus(member.get()));
    }

    @Override
    public ResponseEntity<Void> memberMemberIdStatePut(Integer memberId, MemberStatusDto memberStatusDto) {
        logger.trace("PUT member status request, id: " + memberId);
        Optional<Member> member = membersRepo.findById(memberId);

        if (member.isEmpty()) {
            logger.info("Member " + memberId + " not found.");
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Member not found."
            );
        }

        member.get().setStatus(memberStatusDto.getStatus());
        member.get().setLicenses(Member.toLicenses(memberStatusDto.getLicenses()));

        membersRepo.save(member.get());


        final URI memberUri =
                ServletUriComponentsBuilder.fromCurrentContextPath().path("/member/").path(memberId.toString()).build().toUri();

        URIMemberStatusDto uriStatus = new URIMemberStatusDto();

        var newStatus = new cz.fel.storedav.swa.reservations_client.model.MemberStatusDto();
        newStatus.setLicenses(memberStatusDto.getLicenses());
        newStatus.setStatus(memberStatusDto.getStatus());

        uriStatus.setStatus(newStatus);
        uriStatus.setUri(memberUri);

        reservationsServiceApiClient.notifyChangeMemberPost(uriStatus);

        return ResponseEntity.accepted().build();
    }
}